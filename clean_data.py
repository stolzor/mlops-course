from pathlib import Path
import pandas as pd

FILE_NAME = "all_v2.csv"
CUR_DIR = Path(__file__).resolve().parent
DATA_DIR = CUR_DIR / "data/"
FILE_PATH = DATA_DIR / FILE_NAME

MIN_AREA = 15
MAX_AREA = 300

MIN_PRICE = 1_000_000
MAX_PRICE = 100_000_000

MIN_KITCHEN = 3
MAX_KITCHEN = 70


def clean_data(df: pd.DataFrame):
    df.drop("time", axis=1, inplace=True)
    df["date"] = pd.to_datetime(df["date"])
    df["rooms"] = df["rooms"].apply(lambda x: 0 if x < 0 else x)
    df["price"] = df["price"].abs()
    df = df[(df["area"] <= MAX_AREA) & (df["area"] >= MIN_AREA)]
    df = df[(df["price"] <= MAX_PRICE) & (df["area"] >= MIN_PRICE)]
    df.loc[
        (df["kitchen_area"] >= MIN_KITCHEN) | (df["kitchen_area"] <= MAX_KITCHEN),
        "kitchen_area",
    ] = 0
    area_mean, kitchen_mean = df[["area", "kitchen_area"]].quantile(0.5)
    kitchen_share = kitchen_mean / area_mean
    df.loc[(df["kitchen_area"] == 0) & (df["rooms"] != 0), "kitchen_area"] = (
        df.loc[(df["kitchen_area"] == 0) & (df["rooms"] != 0), "area"] * kitchen_share
    )

    return df


if __name__ == "__main__":
    data = pd.read_csv(FILE_PATH)
    data = data.pipe(clean_data)
    data.to_csv(DATA_DIR / "data_cleaned.csv")
